<?php header("Content-Type: text/html; charset=utf-8"); ?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <!-- Basic Page Needs
      ================================================== -->
    <meta charset="utf-8">
    <title>Photoproduction</title>
    <meta name="description" content="6D Полный цикл создания онлайн контента">
    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS
      ================================================== -->
    <!-- Bootstrap  -->
    <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- web font  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet" type="text/css">
    <!-- plugin css  -->
    <link rel="stylesheet" type="text/css" href="js-plugin/animation-framework/animate.css"/>
    <link rel="stylesheet" type="text/css" href="js-plugin/pretty-photo/css/prettyPhoto.css"/>
    <link type="text/css" rel="stylesheet" href="js-plugin/isotope/css/style.css">
    <!-- icon fonts -->
    <link type="text/css" rel="stylesheet" href="font-icons/custom-icons/css/custom-icons.css">
    <link type="text/css" rel="stylesheet" href="font-icons/custom-icons/css/custom-icons-ie7.css">
    <!-- Custom css -->
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" id="colors" rel="stylesheet" href="css/colors.css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
    <script src="js/modernizr-2.6.1.min.js"></script>
    <!-- Favicons
      ================================================== -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">
</head>
<body data-spy="scroll" data-target="#resMainMenu" data-offset="150">
<!-- Primary Page Layout 
  ================================================== -->
<!-- globalWrapper -->
<div id="globalWrapper" class="localscroll">

<!-- header -->
<header id="mainHeader" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="span12">
                <a href="/" class="brand"><img src="images/main-logo.png" alt="ALTEA website template"/></a>
                <nav id="resMainMenu" class="scrollMenu">
                    <ul class="nav clearfix">
                        <li class="active"><a href="#photography">Фотостудия</a></li>
                        <li><a href="#retouch">Ретушь</a></li>
                        <li><a href="#content">Контент</a></li>
                        <li><a href="#three-d">3D</a></li>
                        <li><a href="#contacts">Контакты</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- header -->

<!-- content -->
<section id="home">
    <div class="photoBox">
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/01.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/02.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/03.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/04.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/05.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_01.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_02.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_03.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/06.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/07.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/08.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/09.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_04.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_05.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/big_06.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/10.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/11.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/12.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div class="bgIMG" style="background: url('/images/home/13.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item">
            <div style="background: url('/images/home/6d.jpg') no-repeat scroll 0% 0% transparent;"></div>
        </span>
        <span class="item"></span>
        <span class="item"></span>
        <span class="item"></span>
    </div>
</section>


<section id="photography">
    <div class="container">
        <div class="row">
            <div class="span4 imgWrapper">
                <img src="images/photos/photography.jpg" alt="" />
            </div>
            <div class="span8">
                <div class="blockTitle">
                    <h2 class="color6">Фотостудия</h2>
                </div>
                <div class="blockText">
                    <p> Основная преграда, останавливающая онлайн-покупателей, - невозможность взять товар в руки,
                        рассмотреть со всех сторон, оценить плюсы и минусы. </p>
                    <p> Мы, как профессионалы, создаём полное впечатление о товаре. </p>
                    <p> Фотографы, стилисты и ассистенты фотосъёмки работают в 3 современных студиях, оборудованных
                        лучшей техникой. Поэтому для нас нет ограничений в съёмке: нашим объективам доверяют
                        производители обуви, аксессуаров, косметики, одежды класса luxury и даже ювелирных изделий.
                        Каждая фотография проходит качественный post-production, благодаря чему выглядит как обложка
                        глянцевого журнала. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<nav class="arrowsNav">
    <a href="#home" class="navUp color5"><i class="icon-up-open"></i></a>
    <a href="#retouch" class="navDown color6"><i class="icon-down-open"></i></a>
</nav>

<section class="banner">
    <span><img src="../images/slider/slide_01.jpg" alt="" /></span>
</section>

<section id="retouch">
    <div class="container">
        <div class="row">
            <div class="span4 imgWrapper">
                <img src="images/photos/retouch.jpg" alt="" />
            </div>
            <div class="span8">
                <div class="blockTitle">
                    <h2 class="color5">Ретушь</h2>
                </div>
                <div class="blockText">
                    <p> Если Вам нужна действительно качественная обработка фотографий, обращайтесь в нашу студию,
                        и будьте уверены в качестве и надежности наших работ, поскольку мы сотрудничаем только с
                        профессионалами, которые знают и любят свое дело. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<nav class="arrowsNav">
    <a href="#photography" class="navUp color6"><i class="icon-up-open"></i></a>
    <a href="#content" class="navDown color5"><i class="icon-down-open"></i></a>
</nav>

<section class="banner">
    <span><img src="../images/slider/slide_02.jpg" alt="" /></span>
</section>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="span4 imgWrapper">
                <img src="images/photos/content.jpg" alt="" />
            </div>
            <div class="span8">
                <div class="blockTitle">
                    <h2 class="color6">Контент</h2>
                </div>
                <div class="blockText">
                    <p> А наши копирайтеры берут в руки каждую вещь, чтобы ощутить и максимально раскрыть покупателю
                        богатство модели. </p>
                    <p> "Вкусные" тексты и небольшие рекомендации по созданию оригинальных образов гарантируют
                        всегда уникальный контент. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<nav class="arrowsNav">
    <a href="#retouch" class="navUp color5"><i class="icon-up-open"></i></a>
    <a href="#three-d" class="navDown color6"><i class="icon-down-open"></i></a>
</nav>

<section id="three-d">
    <div class="container">
        <div class="row">
            <div class="span4 imgWrapper">
                <img src="images/photos/three-d.jpg" alt="" />
            </div>
            <div class="span8">
                <div class="blockTitle">
                    <h2 class="color5">3D</h2>
                </div>
                <div class="blockText">
                    <p> Особое место в нашей работе занимает 3D-съёмка. Мы делаем несколько фотографий с разных
                        ракурсов, объединяем их в один анимационный ролик, который доступен для просмотра на любом
                        компьютере или мобильном устройстве. Покупатель может рассмотреть товар со всех сторон, как
                        будто выбирает его в магазине. </p>
                    <p> Секрет нашего успеха – командная работа над каждым товаром. Наш коллектив – только опытные
                        специалисты, которые любят свою работу. Именно это позволяет нам оставаться лидерами в своём
                        деле. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<nav class="arrowsNav">
    <a href="#content" class="navUp color6"><i class="icon-up-open"></i></a>
    <a href="#contacts" class="navDown color5"><i class="icon-down-open"></i></a>
</nav>
<section id="three-d-gallery" class="slice color6">
    <div class="container imgHover">
        <div class="row">
            <article class="span4">
                <div id="360frames01" class="imgWrapper frames"></div>
            </article>
            <article class="span4">
                <div id="360frames02" class="imgWrapper frames"></div>
            </article>
            <article class="span4">
                <div id="360frames03" class="imgWrapper frames"></div>
            </article>
        </div>
    </div>
</section>


<section id="contacts">
    <div class="container imgHover">
        <div class="row">
            <div class="span12">
                <div class="blockTitle">
                    <h2 class="color6">Контакты</h2>
                </div>
                <div class="blockText">
                    e-mail: <a href="mailto::info@6d.com.ua" alt="">info@6d.com.ua</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- content -->

<!-- footer -->
<footer>
    <section id="footerRights">
        <div class="container">
            <div class="row">
                <div class="span12">
                    Copyright © 2013 <a title="pugupugu.com" href="http://pugupugu.com/" target="_blank">Pugupugu</a>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- End footer -->
</div>
<!-- global wrapper -->

<!-- End Document 
  ================================================== -->
<script type="text/javascript" src="js-plugin/respond/respond.min.js"></script>
<script type="text/javascript" src="js-plugin/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="js-plugin/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
<!-- third party plugins  -->
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap-carousel-ie.js"></script>
<script type="text/javascript" src="js-plugin/spritespin.js"></script>

<script type="text/javascript" src="js-plugin/easing/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js-plugin/responsive-nav/responsive-nav.min.js"></script>

<script type="text/javascript" src="js-plugin/parallax/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="js-plugin/parallax/js/jquery.localscroll-1.2.7-min.js"></script>
<!-- Custom  -->
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
